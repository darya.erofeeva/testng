package Animal;
import org.testng.Assert;
import org.testng.annotations.Test;

public class animalTest {

    private Cat cat = new Cat();
    private Dog dog = new Dog();

    //positive tests
    @Test
    public void testCat() {
        Assert.assertEquals(cat.sound(), "Cat said meo");
    }

    @Test
    public void testDog() {
        Assert.assertEquals(dog.sound(), "Dog said woof");
    }

    @Test(expectedExceptions = AssertionError.class)
    public void checkAnimal() {
    Assert.assertEquals("woof",cat.sound());
    Assert.assertEquals("meo",cat.sound());
    }
}





