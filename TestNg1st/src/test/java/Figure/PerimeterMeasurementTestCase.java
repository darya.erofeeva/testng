package Figure;


import org.testng.Assert;
import org.testng.annotations.ExpectedExceptions;
import org.testng.annotations.Test;
import org.testng.internal.ExpectedExceptionsHolder;
import java.rmi.UnexpectedException;

public class PerimeterMeasurementTestCase {


    @Test
    public void checkPerimeter(){
        Calculate circle = new Circle();
            Assert.assertEquals(37.68f, circle.Perimetr(6f));

        }
    @Test(expectedExceptions = IllegalArgumentException.class)
    public void checkPerimeter2(){
        Calculate circle = new Circle();
        circle.Perimetr(0f);
    }
}
