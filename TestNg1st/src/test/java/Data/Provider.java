package Data;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.util.Strings;

public class Provider {

    @DataProvider(name = "TestData")
    public static Object[][] getData() {
        return new Object[][]{{"String1"}, {"String2"}, {"String3"}, {"String4"}, {"String56789012"}};
    }
/*
    //if number of symbols more then 10
    @Test(dataProvider = "TestData")
    public void testMethod(String data) {
        try {
            Assert.assertTrue(data.length() < 10);

        } catch (AssertionError ae) {
            System.err.println("Some string contains more 10 symbols:"+" "+data);
        }
    }*/

    @Test(dataProvider = "TestData", dataProviderClass = Provider.class)
    public void stringsMethod (String data){
        Assert.assertTrue(data.length()<10);
    }
}



