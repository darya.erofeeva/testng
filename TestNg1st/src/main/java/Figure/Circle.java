package Figure;

public class Circle extends Calculate {
    float pi = 3.14f;
    float radius;


    @Override
    public float Perimetr(float sideSizeOrRadius) {
        this.radius = sideSizeOrRadius;
        if (radius <= 0) {
            throw new IllegalArgumentException();

//            return 2 * pi * radius;
        }
        return 2 * pi * radius;

    }
}



